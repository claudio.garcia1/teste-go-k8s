# Kubernetes All Infra

Este repositório contém todo o ambiente e infraestrutura do ambiente Kubernetes em "[/manifests-k8s](https://gitlab.com/claudio.garcia1/teste-go-k8s/-/tree/master/manifest-k8s)", Docker no arquivo "[Dockerfile](https://gitlab.com/claudio.garcia1/teste-go-k8s/-/blob/master/Dockerfile)" e o arquivo do build automatizado de uma nova imagem do container no arquivo "[.gitlab-ci.yaml](https://gitlab.com/claudio.garcia1/teste-go-k8s/-/blob/master/.gitlab-ci.yml)"


# Etapa 1 - Imagem do Docker

# Como utilizar

Certifique-se de que o docker está instalado em sua maquina e execute o comando abaixo:

```sh
docker build -t bylymonster/teste-claudio:latest .
```   

Aguarde o build da imagem e execute o container com o comando abaixo:

```sh
docker run -d -it --name go-application -p 5000:5000 bylymonster/teste-claudio:latest
```
Por fim, teste os endpoints da aplicação com os comandos abaixo:

```sh
$ curl -i http://127.0.0.1:5000/app
```

```sh
$ curl -i -u myuser:s3cr3t  http://127.0.0.1:5000/admin
```

```sh
$ curl -i http://127.0.0.1:5000/healthz
```

# Etapa 2 - Pipeline e build da imagem docker automaticamente

# Como utilizar 

Configure as seguintes variáveis em seu ambiente de CI/CD para que possa realizar o build automatizado no ambiente de  PRD (utilizando o Docker Registry público "Docker Hub":

| Variável | Obrigatório | Descrição |
| ------ | ------ | ------ |
| CI_REGISTRY_IMAGE | sim | Nome da imagem |
| CI_REGISTRY_PASSWORD | sim | Senha do seu usuário no Docker Hub |
| CI_REGISTRY_USER | sim | Nome de usuário no Dcoker Hub |

Configure as seguintes variáveis em seu ambiente de CI/CD para que possa realizar o build automatizado em seu ambiente de  Staging/hml (utilizando o Docker Registry público "Docker Hub":

| Variaval | Obrigatório | Descrição |
| ------ | ------ | ------ |
| CI_REGISTRY_IMAGE | sim | Nome da imagem |
| CI_REGISTRY_PASSWORD | sim | Senha do seu usuário no Docker Hub |
| CI_REGISTRY_USER | sim | Nome de usuário no Dcoker Hub |

**OBS:** Os builds de Staging/hml serão executados somente quando houver um commit fora da branch master, sendo assim, será gerado uma imagem com a tag de referência ao Commit ID em seu repositório do Docker Hub.


# Etapa 3 - Manifestos do Kubernetes

# Como utilizar

Certifique-se de que seu acesso ao cluster está funcionando e execute o comando abaixo:

```sh
kubectl create -f /manifests-k8s
```   

Verique se todos os recursos foram criados com o comando abaixo:

```sh
kubectl get all -n app-main
```
Por fim, teste os endpoints da aplicação com os comandos abaixo:

```sh
$ curl -i http://127.0.0.1:5000/app
```

```sh
$ curl -i -u myuser:s3cr3t  http://127.0.0.1:5000/admin
```

```sh
$ curl -i http://127.0.0.1:5000/healthz
```

**Caso queira utilizar os ingress para sua aplicação, siga os passos abaixo (utilizando cloud provider AWS):**

Certifique-se de que os serviços da aplicação estejam com o type "NodePort", com o comando abaixo:

```sh
kubectl get svc -n app-main
```
Logo após, edite os arquivos de ingress de acordo com o domínio que será utilizado para cada endpoint:

```sh
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    alb.ingress.kubernetes.io/actions.ssl-redirect: '{"Type": "redirect", "RedirectConfig":
      { "Protocol": "HTTPS", "Port": "443", "StatusCode": "HTTP_301"}}'
    #alb.ingress.kubernetes.io/certificate-arn: <seu-certificateARN-na-AWS> #excluir essa linha caso nao for utilizar certificado
...
spec:
 ...
  rules:
  - host: http://adm.teste-claudio.com #edite aqui de acordo com sua URl
    http:
      paths:
      - backend:
          serviceName: application-service 
          servicePort: 5000
        path: /admin

```
Após editar os **3** arquivos de ingress de acordo com suas URL's de destino, execute o comando abaixo:

```sh
kubectl create -f /ingress
```   

# Etapa 4 - Monitoramento, alertas, logs e afins

# Soluções:

**Monitoramento, alertas e afins**

Para o seguinte monitoramento iremos utilizar um repositorio do helm com os monitoramentos de:

- Pods
- Nodes
- Cluster (num contexto geral)
- Consumo de recursos
- Ciclo de vida
- Persistências dos dados para fins de auditorias

Esse repositório conta como principais ferramentas para coletas de metricas (prometheus), visualização de metricas (grafana) e alertas/notificações (alertmanager)

Como utilizar 


**Certifique-se de que tenha o helm instalado em sua maquina, verifique se sua versão atual é a 3 para prosseguir.**

Adicione o repositório das aplicações estáveis ao seu helm

```sh
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo update
``` 

Crie o namespace monitoring no cluster

```sh
kubectl create ns monitoring
```
Altere os tipos de serviços dos pods do grafana, prometheus de acordo com sua necessidade, adicione ou remova o webhook de integração do alertmanager com slack e senha do grafana, em nosso arquivo de [values](https://gitlab.com/claudio.garcia1/teste-go-k8s/-/blob/master/manifest-k8s/monitoring/values.yaml) da stack de monitoramento, siga abaixo um exemplo de arquivo [values](https://gitlab.com/claudio.garcia1/teste-go-k8s/-/blob/master/manifest-k8s/monitoring/values.yaml) e a explicação dos compos a serem alterados:

**OBS:** Um exclarecimento maior sobre a persistencia de dados encontra-se no arquivo exemplo abaixo.

```sh
coreDns:
  enabled: true

kubeDns:
  enabled: true

alertmanager:
  enabled: true
  config:
    global:
      resolve_timeout: 5m
      slack_api_url: 'https://hooks.slack.com/services/webhook-do-seu-slack'  #defina aqui o seu webhook do slack para receber as notificações por slack

      smtp_smarthost: 'your_smtp_smarthost:587' #    caso deseje enviar notificações via smtp
      smtp_from: 'your_smtp_from'               #         atribua as configurações abaixo
      smtp_auth_username: 'your_smtp_user'      #         de acordo com o seu servidor smtp
      smtp_auth_password: 'your_smtp_pass'      #
    templates:
    - '/etc/alertmanager/template/*.tmpl'
    route:
     group_by: ['alertname', 'cluster', 'service']
     group_wait: 30s
     group_interval: 5m
     repeat_interval: 1h
     receiver: slack
     routes:
     - receiver: 'null'
       match:
         alertname: Watchdog     
     - receiver: blackhole
       match:
         severity: warning #defina a severidade irá para o pager EX: warnig (ignora alertas de warning pra baixo)         
     - receiver: slack
    receivers:
    - name: slack
      slack_configs:
      - channel: '#canal-slack' #coloque o nome de seu canal do slack
        send_resolved: true
        icon_url: https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Kubernetes_logo_without_workmark.svg/220px-Kubernetes_logo_without_workmark.svg.png
        username: 'Cluster-PRD' #Defina o nome para que possa identificar qual cluster esta chagndo alerta em seu slack
        title: |-
          [{{ .Status | toUpper }}{{ if eq .Status "firing" }}:{{ .Alerts.Firing | len }}{{ end }}] {{ .CommonLabels.alertname }} for {{ .CommonLabels.job }}
          {{- if gt (len .CommonLabels) (len .GroupLabels) -}}
            {{" "}}(
            {{- with .CommonLabels.Remove .GroupLabels.Names }}
              {{- range $index, $label := .SortedPairs -}}
                {{ if $index }}, {{ end }}
                {{- $label.Name }}="{{ $label.Value -}}"
              {{- end }}
            {{- end -}}
            )
          {{- end }}
        text: >-
          {{ with index .Alerts 0 -}}
            :chart_with_upwards_trend: *<{{ .GeneratorURL }}|Graph>*
            {{- if .Annotations.runbook }}   :notebook: *<{{ .Annotations.runbook }}|Runbook>*{{ end }}
          {{ end }}

          *Alert details*:

          {{ range .Alerts -}}
            *Alert:* {{ .Annotations.title }}{{ if .Labels.severity }} - `{{ .Labels.severity }}`{{ end }}
          *Description:* {{ .Annotations.description }}
          *Details:*
            {{ range .Labels.SortedPairs }} • *{{ .Name }}:* `{{ .Value }}`
            {{ end }}
          {{ end }}
      email_configs:
      - to: 'your_alert_email_address' #defina neste campo o email ao qual deseja enviar notificações/alertas
        send_resolved: true    
    - name: blackhole
    - name: 'null'
  
  alertmanagerSpec:
    replicas: 2
    storage:
      volumeClaimTemplate:  #abaixo estao todas as definições da persistencias de dados do alertmanager para que possamos ter um historico de alertas
        spec:
          storageClassName: gp2      
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 5Gi


prometheus:
  prometheusSpec:
    serviceMonitorNamespaceSelector: {}
    retention: 30d  #aqui definimos um tempode retenção de dados de 30 dias para que o prometheus faça o recycle do dados para nao sobrecarregar o disco , podendo assim aumentar ou diminuir este valor em dias para e aumentando o disco do persistent volume para comportar um maior volume de dados
    replicas: 1
    storageSpec:
      volumeClaimTemplate: #abaixo estao todas as definições da persistencias de dados do prometheus para que possamos ter um historico de alertas e metricas 
        spec:
          storageClassName: gp2 #gp2= aws |standard= gcp |managed-premium= azure     
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 25Gi
  service:
    type: LoadBalancer #podendo aternar entre os types LoadBalancer,ClusterIP ou NodePort de acordo com o seu ambiente
    port: 9090
    targetPort: 9090
    annotations: {}
    labels: {}

grafana:
  adminUser: admin
  adminPassword: q1w2e3r4 #altere a senha para uma maior segurança de seu ambiente

  persistence:
    storageClassName: gp2 #gp2= aws |standard= gcp |managed-premium= azure
    enabled: true
    accessModes: ["ReadWriteOnce"]
    size: 50Gi

  service:
    type: LoadBalancer #podendo aternar entre os types LoadBalancer,ClusterIP ou NodePort de acordo com o seu ambiente 
    port: 3000
    targetPort: 3000
    annotations: {}
    labels: {}
```
Por fim execute o comando abaixo:

```ssh
helm install monitoring stable/prometheus -f monitoring/values.yaml -n monitoring
```

# Motivos para a utilização dessa stack de monitoramento:
Prometheus:
- Executado em time series, sendo assim, entregando metricas praticamente em tempo real com baixo delay
- Consome poucos recursos para monitorar praticamete todo o cluster
- É facilmente configurável com novos inputs/targets de metricas em seus datasources
- Possui uma gama gigantesca de monitoramentos, de clusters kubernetes á URL's e certificados ssl

Grafana:
- Com gráficos e plugins para um visualização mais intuitiva, ele deixa a aplicação de novasmetricas facilidata ao usuário
- Possui diversos datasources que podemos integrar à ele, sendo assim , podendo ser utilizados para visualização de metricas de um prometheus ou ate mesmo um zabbix server e afins.
- Facilidade no controle de usuários e visualização de cada um deles, podendo alternar diversos tipos de autenticação como LDAP e afins
- Consome poucos recursos e etrega um ampla cobertura de usuários simultâneos

AlertManager:
- Facil integração com diversos serviços de chat e afins, tais como: Slack, MSTeams, PagerDuty e etc.
- Consome poucos recursos em sua utilização pois o mesmo será utilizado somente para notificações, mesmo contendo grandes mensagens.


**Logs**

Para o seguinte monitoramento iremos utilizar uma ferramenta oficial do kubernetes o "Kubernetes Dashboard" (utilizando EKS do cloud provider AWS):

- Pods
- Nodes
- Cluster (num contexto geral)
- Consumo de recursos
- Ciclo de vida
- Persistências dos dados para fins de auditorias
- Logs

Esse repositório conta como principal ferramenta para coletas de metricas (Kubernetes Dashboard).

**Certifique-se de que tenha o "Metrics Server" instalado no cluster, verifique com o comando abaixo**

```sh
kubectl top pods -A
```
caso obtiver a saída "error: Metrics API not available" execute o comando abaixo:

```sh
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml
```

Com o Metrics server instalado execute o comando abaixo para provisioar o ambiente do "Kubernetes Dashboard":

```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
```

Aguarde o provisionamento de todos os recurso e voce receberá a seguinte saída:

```sh
namespace/kubernetes-dashboard created
serviceaccount/kubernetes-dashboard created
service/kubernetes-dashboard created
secret/kubernetes-dashboard-certs created
secret/kubernetes-dashboard-csrf created
secret/kubernetes-dashboard-key-holder created
configmap/kubernetes-dashboard-settings created
role.rbac.authorization.k8s.io/kubernetes-dashboard created
clusterrole.rbac.authorization.k8s.io/kubernetes-dashboard created
rolebinding.rbac.authorization.k8s.io/kubernetes-dashboard created
clusterrolebinding.rbac.authorization.k8s.io/kubernetes-dashboard created
deployment.apps/kubernetes-dashboard created
service/dashboard-metrics-scraper created
deployment.apps/dashboard-metrics-scraper created
```

Com todos os recursos criados, execute o comando abaixo para criar o service account e o cluster role binding para que possa acessar o Dashboard, utlize o arquivo "[eks-admin-service-account.yaml](https://gitlab.com/claudio.garcia1/teste-go-k8s/-/blob/master/manifest-k8s/kubernetes-dashboard/eks-admin-service-account.yaml)":

```sh
kubectl apply -f eks-admin-service-account.yaml
```
**Saída:**
```sh
serviceaccount "eks-admin" created
clusterrolebinding.rbac.authorization.k8s.io "eks-admin" created
```

Para conectar no Dashboard precisaremos do token de acesso gerado ao criados nosso cluster role binding, para isso, execute o comando abaixo:

```sh
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
```
**Saída:**
```sh
Name:         eks-admin-token-b5zv4
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name=eks-admin
              kubernetes.io/service-account.uid=c5c2a989-6876-43ab-9940-6c57db64fca3

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1025 bytes
namespace:  11 bytes
token:      <authentication_token>  #copie seu token daqui
```

Com seu token em mãos exe o comando abaixo para acessar o Dashboard:

```sh
kubectl proxy
```
Entao entre na URL abaixo para acessa-lo:

```sh
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login
```
Por fim clique a opção "Token" e cole seu token no campo "Enter token"

# Motivos para a utilização do "Kubernetes Dashboard":
- Dashboard intuitivo
- Ferramenta oficial do kubernetes
- Facilidade de alterar recursos no cluster sem precisar conectar somente pelo terminal
- Fácil administração de acesso e permissões dos recursos dos usuários no cluster
- Visualização de logs diretamente do dashboard
- Gráficos de consumo dos recursos do cluster
- Fácil Instalação e configuração
- Consome poucos recursos levando em conta a sua entrega de conteúdo.


# Etapa 5 - Frontend

# Soluções:

Utilizando o cloud provider aws criaremos um "Bucket S3" para o provisionamento de nossa aplicação estática junto do "Cloud Front", para que possamos utilizar o metodo de "Origin Path" e assim direcionarmos para os paths de nossas aplicações, após isso, configuraremos o "Route 53" para que o dominio compartilhado possa ser utilizado nesta aplicação mas tambem nas demais, atravez de um "simple record" do nosso dominio raiz, veja abaixo o detalhamento de cada abordagem:

# Bucket S3: No "Bucket S3" colocaremos todos os arquivos de nossa aplicação estatica seguindo os passo abaixo:
1. Vá em "Properties">"Static website hosting">"edit".
2. Depois em "Static website hosting">"Enable".
3. E clique em "Hosting type">"Host a static website".
4. No campo "Index document" iremos colocar o caminho de nossa pagina index dentro do "Bucket S3".
5. Se tivermos uma pagina de error, no campo "Error document" devemos colocar o caminho de nossa pagina de error dentro do "Bucket S3".
6. Por fim clique em "Save changes"
7. Após isso vamos adicionar as politicas ao nosso "Bucket S3", vá em "Permissions">"Bucket policy">"Edit" e adicione a politica abaixo no campo "Policy" e coloque o nome do seu bucket em "bucket-name".
```sh
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::bucket-name/*"
        }
    ]
}
```
8. Por fim clique em "Save changes"
9. Para que o acesso fique publico a todos os usuário deveremos mudar as "Block public access (bucket settings)" de nosso "Bucket S3", vá em "Permissions">"Block public access (bucket settings)">"Edit"
10. Desmarque todas as caixas de seleção e deixe-as em branco
11. Clique em "Save changes" e confirme a mudança.

# Motivos da utilização do serviço "Bucket S3" do cloud Provider "AWS"
- Baixo custo
- Ampla disponibilidade de armazenamento
- Não requer nenhum web server especifico, portanto o Provisionamento de aplicações fica simplificado
- Baixa latência de rede

# Cloud Front: No "Cloud Front" Iremos definir o Path Origin ao qual nosso "Bucket S3" respondera, assim podendo acessar a aplicação por cada path correspondente, siga os passo abaixo:
1. Primeiramente va em "Cloud Front" no console da "AWS", depois em "Create Distribution"
2. Na opção "Web", clique em "Get Started"
3. No campo "Origin Domain Name" defina o nome de origem, neste caso será a nossa aplicação que esta no "Bucket S3", vá em "Properties">"Static website hosting" e copie o link do seu "Bucket S3" e cole no campo "Origin Domain Name" na criação do "Cloud Front"
4. Em "Origin Path" o Path onde queremos que nossa aplicação responda ao "Cloud Front"
5. Vá em "Distribution Settings">"Alternate Domain Names(CNAMEs)" e adicione o dominio ou subdominio a ser utilizado pelo "Cloud Front"
6. Por fim clique em "Create Distribution"
7. Para adicionar main de um Path da nossa aplicação, vá no painel do "Cloud Front" depois clique em sua distribuição, vá em "Origins and Origin Groups">"Create Origin"
8. No campo "Origin Domain Name" repita o processo do passo **3** e cole 
8. No Campo "Origin Path" o outro Path onde queremos que nossa aplicação responda ao "Cloud Front" 
9. Clique em "Create" para adicionar o novo Path 

**OBS:** Repita o processo até quantos Paths você tiver

# Motivos da utilização do serviço "Cloud Front" do cloud Provider "AWS"
- Baixo custo
- Podemos agregar certificado ssl assinado facilmente
- Não requer nenhum web server especifico, portanto o Provisionamento de aplicações fica simplificado
- Baixa latência de rede
- Facil inserção de domínio (compartilhado ou dedicado)
- Uso do "CDN" (cahe de seções) para um melhor desempenho da aplicação


# Route 53: No "Route 53" Iremos definir o domínio ao qual nosso "Cloud Front" responderá, assim podendo acessar a aplicação por um domínio por cada path correspondente, siga os passo abaixo:
1. Primeiramente va em "Route 53" no console da "AWS", depois em "Create hosted zone"
2. No campo "Domain name" insira o seu dominio
3. E por fim clique em "Create hosted zone"
4. Após criada a sua zona de DNS no "Route 53", clique nela e depois em "Create record"
5. Clique em "Next'
6. clique em "Define simple record"
7. No campo "Record name" defina o nome do subdomínio que deseja criar
8. Depis em "Value/Route traffic to", selecione o seu "Cloud front criado anteriormente", selecionando a opção "Alias to CloudFront Distribution"
9. No campo abaixo selecione a Região em que o "Cloud Front" de sua aplicação se encontra
10. Abaixo selecione o "Cloud Front" da sua aplicação
11. Por fim clique em "Define simple Record"

# Motivos da utilização do serviço "Route 53" do cloud Provider "AWS"
- Sendo do mesmo cloud provider a integração entre os serviços fica facilitada
- A aws fornece um meio de notificações para quando seu dominio estiver proximo de expirar
- Facilidade no gerenciamento de suas zonas de DNS e facil criação de subdomínios
- Custo anual padrão de um domínio
- Custo adicional **ZERO** além do custo de aquisição padrão de qualquer domínio
