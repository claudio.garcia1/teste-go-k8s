FROM golang:latest

LABEL maintainer="Claudio Gabriel Villar <claudiogabriel17@gmail.com>"

WORKDIR /

RUN wget https://raw.githubusercontent.com/magnetis/infra-app/master/main.go

RUN go build -o app

EXPOSE 5000

ENTRYPOINT PORT=5000 ADMIN_USER=myuser ADMIN_PASS=s3cr3t ./app 